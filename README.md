# Android Java SDK

## ¿Como instalar?

Para obtener la versión de SDK con Java/Android es necesario:

Ir a la ruta {proyecto}/build.gradle y agregar en la sección de repositories lo siguiente:

``` gradle
allprojects {
	repositories {
		...
		maven { url 'https://jitpack.io' }
	}
}
```

El siguiente paso es dirigirte a la ruta {proyecto}/app/build.gradle y en la sección de dependencies agregar:

``` gradle
dependencies {
    .
    .
    implementation 'org.bitbucket.pixelpay:sdk-android:2.0.1'
}
```