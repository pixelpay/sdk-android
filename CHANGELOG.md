# Changelog - PixelPay Java Standalone SDK
Todos los cambios notables de este proyecto se documentarán en este archivo. Los registros de cambios son *para humanos*, no para máquinas, y la última versión es lo primero a mostrar.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
y este proyecto se adhiere a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
Tipos de cambios: `Added`, `Changed`, `Deprecated`, `Removed`, `Fixed`, `Security`.

## [v2.2.5] - 2024-04-01
### Added
- Se agrego mensaje de error descriptivo para cancelación de autenticación.

## [v2.2.4] - 2024-03-04
### Fixed
- Se actualizó el SDK de Java a v2.2.2

## [v2.2.3] - 2024-01-08
### Added
- Se mejora versionado en las peticiones http.

## [v2.2.2] - 2023-10-16
### Changed
- Se actualizó el SDK de Java a v2.2.1

## [v2.2.1] - 2023-09-27
### Changed
- Se actualizó la libreria de Cardinal a v2.2.7-5

## [v2.2.0] - 2023-09-21
### Removed
- Se eliminó lógica de encriptación

## [v2.1.0-beta.0] - 2023-08-08
### Added
- Se agregó encriptación de datos de tarjeta.
- Se agregó campo de cuotas para autenticación con 3D Secure.

### Fixed
- Se arregló excepción para caso 2.12 de 3D Secure.

## [v2.0.3] - 2023-06-09
### Added
- Se agregan campos de cuotas y puntos
- Se agrega firma de transacciones de anulación
- Se agrega función para obtener listado de formatos de teléfono y zip

### Changed
- Se actualiza el SDK de Cardinal Mobile

## [v2.0.2] - 2022-12-06
### Changed
- Se actualizo la libreria de Cardinal a v2.2.7-2
- Se agregaron algunos campos para mejorar la transaccionalidad

## [v2.0.1] - 2022-05-08
### Added
- Se agrega el method `getCybersourceFingerprint` para la integraciones con Promerica o Cybersource
- Se agrega soporte para integraciones con Bac Legacy/FAC

### Fixed
- Correción del error de compilacion con Jackson y sus anotaciones

## [v2.0.0] - 2022-04-28
### Added
- Se publica primer version de pruebas