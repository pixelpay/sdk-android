package com.pixel.sdk.mobile.base;

import android.app.Activity;
import android.app.Dialog;

import com.pixel.sdk.base.Response;

import com.pixel.sdk.mobile.contracts.Catchable;
import com.pixel.sdk.mobile.contracts.Requestable;
import com.pixel.sdk.mobile.contracts.Thennable;
import com.pixel.sdk.mobile.libraries.PixelPayLoading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncResponse {
	/**
	 * Service instance
	 */
	protected Requestable requestable;

	/**
	 * Catchable runnable service
	 */
	protected Catchable catchable;

	/**
	 * Tenable runnable service
	 */
	protected Thennable thennable;

	/**
	 * Android project activity
	 */
	protected Activity current_activity;

	/**
	 * Loading modal
	 */
	protected PixelPayLoading loading;

	/**
	 * Enable loading show
	 */
	protected Boolean show_loading = false;

	/**
	 * Setup service behavior
	 *
	 * @param requestable
	 */
	public AsyncResponse(Requestable requestable, Activity current_activity) {
		this.requestable = requestable;
		this.current_activity = current_activity;
		this.loading = new PixelPayLoading(current_activity);
	}

	/**
	 * Show loading on start request
	 *
	 * @return
	 */
	public AsyncResponse withLoading() {
		this.show_loading = true;

		return this;
	}

	/**
	 * Then runnable hook expression
	 *
	 * @param expression
	 * @return
	 */
	public AsyncResponse then(Thennable expression) {
		this.thennable = expression;

		return this;
	}

	/**
	 * Catch runnable hook expression
	 *
	 * @param expression
	 * @return
	 */
	public AsyncResponse thenCatch(Catchable expression) {
		this.catchable = expression;

		return this;
	}

	/**
	 * Validate Transaction
	 */
	public void start() {
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				try {
					if (Boolean.TRUE.equals(show_loading)) {
						loading.showDialog();
					}

					if (thennable != null) {
						Response response = requestable.toRequest();

						thennable.toThen(response);
					}

					loading.hideDialog();
				} catch (Exception ex) {
					if (catchable != null) {
						catchable.toCatch(ex);
					}

					ex.printStackTrace();

					loading.hideDialog();
				}
			}
		});
	}
}
