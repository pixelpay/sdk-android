package com.pixel.sdk.mobile.contracts;

import com.pixel.sdk.base.Response;

public interface Requestable {
	Response toRequest() throws Exception;
}
