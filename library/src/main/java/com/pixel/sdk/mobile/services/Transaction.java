package com.pixel.sdk.mobile.services;

import android.app.Activity;

import com.pixel.sdk.base.ServiceBehaviour;
import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.mobile.contracts.Gettable;
import com.pixel.sdk.mobile.libraries.CybersourceManager;
import com.pixel.sdk.mobile.libraries.PayloadManager;
import com.pixel.sdk.mobile.utils.Version;
import com.pixel.sdk.requests.AuthTransaction;
import com.pixel.sdk.requests.CaptureTransaction;
import com.pixel.sdk.requests.SaleTransaction;
import com.pixel.sdk.requests.StatusTransaction;
import com.pixel.sdk.requests.VoidTransaction;
import com.pixel.sdk.mobile.base.AsyncResponse;
import com.pixel.sdk.mobile.libraries.CardinalManager;
import com.pixel.sdk.models.Settings;

public class Transaction extends ServiceBehaviour {
	/**
	 * Android project activity
	 */
	protected Activity current_activity;

	/**
	 * Parent Transaction service
	 */
	private final com.pixel.sdk.services.Transaction parent_service;

	/**
	 * Initialize instance
	 *
	 * @param settings
	 * @throws InvalidCredentialsException
	 */
	public Transaction(Settings settings, Activity current_activity) {
		super(settings);
		this.current_activity = current_activity;
		this.parent_service = new com.pixel.sdk.services.Transaction(settings);

		if (this.settings.sdk == null) {
			this.settings.sdk = "sdk-android";
		}

		if (this.settings.sdk_version == null) {
			this.settings.sdk_version = Version.SDK_VERSION;
		}
	}

	/**
	 * Send and process async SALE transaction
	 *
	 * @param transaction
	 * @return
	 */
	public AsyncResponse doSale(SaleTransaction transaction) {
		if (transaction.authentication_request) {
			return new CardinalManager(() -> this.parent_service.doSale(transaction), this.settings,
					this.current_activity, transaction).withLoading();
		} else {
			return new PayloadManager(() -> this.parent_service.doSale(transaction),
					this.current_activity).withLoading();
		}
	}

	/**
	 * Send and process async AUTH transaction
	 *
	 * @param transaction
	 * @return
	 */
	public AsyncResponse doAuth(AuthTransaction transaction) {
		if (transaction.authentication_request) {
			return new CardinalManager(() -> this.parent_service.doAuth(transaction), this.settings,
					this.current_activity, transaction).withLoading();
		} else {
			return new PayloadManager(() -> this.parent_service.doAuth(transaction),
					this.current_activity).withLoading();
		}
	}

	/**
	 * Send and process async CAPTURE transaction
	 *
	 * @param transaction
	 * @return
	 */
	public AsyncResponse doCapture(CaptureTransaction transaction) {
		return new AsyncResponse(() -> this.parent_service.doCapture(transaction), this.current_activity).withLoading();
	}

	/**
	 * Send and process async VOID transaction
	 *
	 * @param transaction
	 * @return
	 */
	public AsyncResponse doVoid(VoidTransaction transaction) {
		return new AsyncResponse(() -> this.parent_service.doVoid(transaction), this.current_activity).withLoading();
	}

	/**
	 * Verify transaction status
	 *
	 * @param transaction
	 * @return
	 */
	public AsyncResponse getStatus(StatusTransaction transaction) {
		return new AsyncResponse(() -> this.parent_service.getStatus(transaction), this.current_activity);
	}

	/**
	 * Verify a payment hash and returns true if payment response is not modified
	 *
	 * @param hash
	 * @param order_id
	 * @param secret
	 * @return
	 */
	public boolean verifyPaymentHash(String hash, String order_id, String secret) {
		return this.parent_service.verifyPaymentHash(hash, order_id, secret);
	}

	/**
	 * Get Cybersource Decision Manager Fingerprint
	 *
	 * @param merchant_id
	 * @param org_id
	 * @param callback
	 */
	public void getCybersourceFingerprint(String merchant_id, String org_id, Gettable callback) {
		CybersourceManager.getFingerprint(current_activity, merchant_id, org_id, callback);
	}
}
