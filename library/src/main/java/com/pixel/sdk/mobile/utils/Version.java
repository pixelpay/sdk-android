package com.pixel.sdk.mobile.utils;

public class Version {
    /**
     * The version of the SDK
     */
    public static final String SDK_VERSION = "2.2.5";
}