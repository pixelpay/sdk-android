package com.pixel.sdk.mobile.services;

import android.app.Activity;

import com.pixel.sdk.base.Response;
import com.pixel.sdk.base.ServiceBehaviour;
import com.pixel.sdk.exceptions.InvalidTransactionTypeException;
import com.pixel.sdk.mobile.requests.LookupContinueTransaction;
import com.pixel.sdk.mobile.requests.LookupTransaction;
import com.pixel.sdk.mobile.utils.Version;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.requests.AuthTransaction;
import com.pixel.sdk.requests.PaymentTransaction;
import com.pixel.sdk.requests.SaleTransaction;

public class CardinalAuthentication extends ServiceBehaviour {
	/**
	 * Android project activity
	 */
	protected Activity current_activity;

	/**
	 * Initialize instance
	 *
	 * @param settings
	 * @throws InvalidCredentialsException
	 */
	public CardinalAuthentication(Settings settings, Activity current_activity) {
		super(settings);
		this.current_activity = current_activity;

		if (this.settings.sdk == null) {
			this.settings.sdk = "sdk-android";
		}

		if (this.settings.sdk_version == null) {
			this.settings.sdk_version = Version.SDK_VERSION;
		}
	}

	/**
	 * Send an authentication lookup transaction
	 */
	public Response authenticationLookup(LookupTransaction transaction) throws InvalidCredentialsException {
		return this.post("api/v2/cardinal/authentication/lookup", transaction);
	}

	/**
	 * Send and validate authentication continue transaction
	 */
	public Response authenticationContinue(LookupContinueTransaction transaction) throws InvalidCredentialsException {
		return this.post("api/v2/cardinal/authentication/continue", transaction);
	}

	/**
	 * Send payment transaction by type (Sale/Auth)
	 */
	public Response retryTransaction(PaymentTransaction transaction)
			throws InvalidCredentialsException, InvalidTransactionTypeException {
		if (transaction instanceof SaleTransaction) {
			return this.post("api/v2/transaction/sale", transaction);
		} else if (transaction instanceof AuthTransaction) {
			return this.post("api/v2/transaction/auth", transaction);
		} else {
			throw new InvalidTransactionTypeException("The request payment type is invalid.");
		}
	}
}
