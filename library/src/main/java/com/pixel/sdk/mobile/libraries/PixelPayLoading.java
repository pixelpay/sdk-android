package com.pixel.sdk.mobile.libraries;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.view.WindowManager;

import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import com.pixel.sdk.mobile.R;

public class PixelPayLoading {
	/**
	 * Currency activity
	 */
	protected Activity activity;

	/**
	 * Main loading dialog
	 */
	protected static Dialog dialog;

	/**
	 * Build dialog
	 *
	 * @param activity
	 */
	public PixelPayLoading(Activity activity) {
		this.activity = activity;
	}

	/**
	 * Show loading dialog
	 */
	public void showDialog() {
		(new Handler(Looper.getMainLooper())).post(new Runnable() {
			@Override
			public void run() {
				dialog = new Dialog(activity);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCancelable(false);
				dialog.setContentView(R.layout.loading);

				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

				dialog.show();
			}
		});
	}

	/**
	 * Hide loading dialog
	 */
	public void hideDialog() {
		if (this.dialog != null) {
			dialog.dismiss();
		}
	}
}
