package com.pixel.sdk.mobile.libraries;

import android.app.Activity;

import com.cardinalcommerce.cardinalmobilesdk.Cardinal;
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalEnvironment;
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalRenderType;
import com.cardinalcommerce.cardinalmobilesdk.enums.CardinalUiType;
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalActionCode;
import com.cardinalcommerce.cardinalmobilesdk.models.CardinalConfigurationParameters;
import com.cardinalcommerce.cardinalmobilesdk.models.ValidateResponse;
import com.cardinalcommerce.cardinalmobilesdk.services.CardinalInitService;
import com.pixel.sdk.base.Response;
import com.pixel.sdk.exceptions.InvalidCredentialsException;
import com.pixel.sdk.exceptions.InvalidTransactionTypeException;
import com.pixel.sdk.requests.PaymentTransaction;
import com.pixel.sdk.mobile.base.AsyncResponse;
import com.pixel.sdk.mobile.contracts.Requestable;
import com.pixel.sdk.mobile.requests.LookupContinueTransaction;
import com.pixel.sdk.mobile.requests.LookupTransaction;
import com.pixel.sdk.mobile.services.CardinalAuthentication;
import com.pixel.sdk.resources.Environment;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.responses.PayloadResponse;

import org.json.JSONArray;
import java.util.concurrent.Executors;

public class CardinalManager extends AsyncResponse {
	/**
	 * Cardinal authentication service
	 */
	public static CardinalAuthentication service;

	/**
	 * Payment transaction
	 */
	public static PaymentTransaction transaction;

	/**
	 * Service settings
	 */
	protected Settings settings;

	/**
	 * Obtain CardinalSDK instance
	 */
	protected static Cardinal client;

	/**
	 * Initialize manager
	 */
	public CardinalManager(Requestable requestable, Settings settings, Activity activity, PaymentTransaction request) {
		super(requestable, activity);

		this.transaction = request;
		this.settings = settings;
	}

	/**
	 * Setup Cardinal session service
	 */
	private void setupCardinalSession(Settings settings) {
		CardinalConfigurationParameters cardinalConfigurationParameters = new CardinalConfigurationParameters();
		cardinalConfigurationParameters.setEnvironment(
				(Environment.SANDBOX.equals(settings.environment) || Environment.STAGING.equals(settings.environment))
						? CardinalEnvironment.STAGING
						: CardinalEnvironment.PRODUCTION);

		JSONArray rType = new JSONArray();
		rType.put(CardinalRenderType.OTP);
		rType.put(CardinalRenderType.SINGLE_SELECT);
		rType.put(CardinalRenderType.MULTI_SELECT);
		rType.put(CardinalRenderType.OOB);
		rType.put(CardinalRenderType.HTML);
		cardinalConfigurationParameters.setRenderType(rType);

		cardinalConfigurationParameters.setUiType(CardinalUiType.BOTH);
		this.client.configure(this.current_activity.getApplicationContext(), cardinalConfigurationParameters);
	}

	/**
	 * Cardinal transaction starting service
	 */
	private void startCardinalTransaction(String jwt) {
		this.client.init(jwt, new CardinalInitService() {
			@Override
			public void onSetupCompleted(String consumerSessionId) {
				LookupTransaction lookup = new LookupTransaction();
				lookup.fromPaymentTransaction(transaction);
				lookup.reference = consumerSessionId;

				try {
					Response response = service.authenticationLookup(lookup);
					validationLookupResponse(response);
				} catch (Exception e) {
					e.printStackTrace();

					if (catchable != null) {
						catchable.toCatch(e);
					}

					loading.hideDialog();
				}
			}

			@Override
			public void onValidated(ValidateResponse validateResponse, String serverJwt) {
				if (catchable != null) {
					catchable.toCatch(new Exception("Cardinal initial setup error. "
							+ validateResponse.getErrorDescription() + " (" + validateResponse.getErrorNumber() + ")"));
				}

				loading.hideDialog();
			}
		});
	}

	/**
	 * Verify lookup response and run Cardinal attempt
	 */
	private void validationLookupResponse(Response response)
			throws InvalidCredentialsException, InvalidTransactionTypeException {
		if (!response.success) {
			if (catchable != null) {
				catchable.toCatch(new Exception(response.message));
			}

			loading.hideDialog();

			return;
		}

		String identifier = String.valueOf(response.getData("identifier"));
		transaction.authentication_identifier = identifier;

		if (response.action != null && response.action.equals("continue")) {
			String continue_payload = String.valueOf(response.getData("continue_payload"));
			String transaction_id = String.valueOf(response.getData("transaction_id"));

			client.cca_continue(transaction_id, continue_payload, current_activity,
					(currentContext, validateResponse, jwt) -> {
						try {
							if (jwt != null && !jwt.isEmpty()) {
								LookupContinueTransaction lookup_continue = new LookupContinueTransaction();
								lookup_continue.identifier = identifier;
								lookup_continue.payload = jwt;

								service.authenticationContinue(lookup_continue);
								sendPaymentTransactionWithAuthenticationResults();
							} else if (validateResponse.getActionCode() == CardinalActionCode.NOACTION) {
								sendPaymentTransactionWithAuthenticationResults();
							} else {
								if (catchable != null) {
									Exception ex = new Exception(validateResponse.getErrorDescription() + " ("
											+ validateResponse.getErrorNumber() + ")");

									if (validateResponse.getActionCode() == CardinalActionCode.CANCEL) {
										ex = new Exception("Authentication process canceled by user ("
												+ validateResponse.getErrorNumber() + ")");
									}

									catchable.toCatch(ex);
								}

								loading.hideDialog();
							}
						} catch (Exception e) {
							e.printStackTrace();

							if (catchable != null) {
								catchable.toCatch(e);
							}

							loading.hideDialog();
						}
					});
		} else {
			sendPaymentTransactionWithAuthenticationResults();
		}
	}

	/**
	 * Send payment transaction by type (Sale/Auth)
	 */
	private void sendPaymentTransactionWithAuthenticationResults()
			throws InvalidTransactionTypeException, InvalidCredentialsException {
		this.thennable.toThen(this.service.retryTransaction(this.transaction));
		loading.hideDialog();
	}

	/**
	 * Validate Transaction
	 */
	public void start() {
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				try {
					if (show_loading) {
						loading.showDialog();
					}

					if (thennable != null) {
						Response response = requestable.toRequest();

						if (response instanceof PayloadResponse) {
							String transaction_type = String.valueOf(response.getData("transaction_type"));
							String jwt = String.valueOf(response.getData("payload"));

							if (transaction_type.equals("songbird")) {
								CardinalManager.this.service = new CardinalAuthentication(settings, current_activity);
								client = Cardinal.getInstance();

								setupCardinalSession(settings);
								startCardinalTransaction(jwt);
							} else {
								thennable.toThen(response);
								loading.hideDialog();
							}
						} else {
							thennable.toThen(response);
							loading.hideDialog();
						}
					}
				} catch (Exception ex) {
					if (catchable != null) {
						catchable.toCatch(ex);
					}

					loading.hideDialog();
				}
			}
		});
	}
}
