package com.pixel.sdk.mobile.requests;

import com.pixel.sdk.base.RequestBehaviour;

public class LookupContinueTransaction extends RequestBehaviour {
	/**
	 * Cardinal transaction identifier
	 */
	public String identifier;

	/**
	 * Cardinal Validation JWT payload
	 */
	public String payload;
}
