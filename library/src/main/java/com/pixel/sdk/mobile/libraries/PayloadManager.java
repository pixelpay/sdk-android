package com.pixel.sdk.mobile.libraries;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pixel.sdk.base.Response;
import com.pixel.sdk.mobile.R;
import com.pixel.sdk.mobile.base.AsyncResponse;
import com.pixel.sdk.mobile.contracts.Requestable;
import com.pixel.sdk.mobile.exceptions.IntegrationException;
import com.pixel.sdk.responses.PayloadResponse;

import java.util.Base64;
import java.util.concurrent.Executors;

public class PayloadManager extends AsyncResponse {
    /**
     * Main webview dialog
     */
    protected static Dialog dialog;

    /**
     * Build dialog
     *
     * @param requestable
     * @param current_activity
     */
    public PayloadManager(Requestable requestable, Activity current_activity) {
        super(requestable,current_activity);
    }

    /**
     * Show webview dialog
     */
    public void showModal(String payload) {
        (new Handler(Looper.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                dialog = new Dialog(current_activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.payload);

                WebView payload_modal = (WebView) dialog.findViewById(R.id.payload_webview);

                WebSettings settings = payload_modal.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setAllowContentAccess(true);

                payload_modal.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url){
                        view.loadUrl(url);
                        return false;
                    }
                });

                payload_modal.addJavascriptInterface(new Object() {
                    /**
                     * Process response from JS
                     *
                     * @param response
                     * @param status
                     */
                    @JavascriptInterface
                    public void processResponse(String response, int status) {
                        System.out.println(Response.fromJson(response, status));
                        hideDialog();
                        thennable.toThen(Response.fromJson(response, status));
                    }
                }, "Android");

                payload_modal.loadData(payloadDecoder(payload), "text/html", "utf8");

                dialog.show();
            }
        });
    }

    /**
     * Hide webview dialog
     */
    public void hideDialog() {
        if (this.dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * Validate Transaction
     */
    public void start() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (Boolean.TRUE.equals(show_loading)) {
                        loading.showDialog();
                    }

                    if (thennable != null) {
                        Response response = requestable.toRequest();

                        if (response instanceof PayloadResponse) {
                            String transaction_type = String.valueOf(response.getData("transaction_type"));
                            String payload = String.valueOf(response.getData("payload"));

                            if (transaction_type.equals("payload")) {
                                showModal(payload);
                            } else if (transaction_type.equals("songbird")) {
                                throw new IntegrationException("It is necessary to enable the 3DS service in this integration, " +
                                        "remember to include the statement withAuthenticationRequest(). " +
                                        "Check the requests section in the documentation.");
                            } else {
                                thennable.toThen(response);
                            }
                        } else {
                            thennable.toThen(response);
                        }
                    }

                    loading.hideDialog();
                } catch (Exception ex) {
                    if (catchable != null) {
                        catchable.toCatch(ex);
                    }

                    ex.printStackTrace();

                    loading.hideDialog();
                }
            }
        });
    }

    /**
     * JSON Payload decoder from Base64
     *
     * @param payload
     * @return
     */
    private String payloadDecoder(String payload) {
        byte[] decode = new byte[0];

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            decode = Base64.getDecoder().decode(payload);
        }

        return new String(decode);
    }
}
