package com.pixel.sdk.mobile.libraries;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.pixel.sdk.mobile.R;
import com.pixel.sdk.mobile.contracts.Gettable;

public class CybersourceManager {
    /**
     * Main webview dialog
     */
    protected static Dialog dialog;

    /**
     * Current activity
     */
    protected Activity current_activity;

    /**
     * Gettable runnable service
     */
    protected Gettable gettable;

    /**
     * Setup service behavior
     *
     * @param current_activity
     */
    public CybersourceManager(Activity current_activity) {
        this.current_activity = current_activity;
    }

    /**
     * Show webview dialog
     */
    public void showDialog(String merchant_id, String org_id, Gettable gettable) {
        this.gettable = gettable;

        (new Handler(Looper.getMainLooper())).post(new Runnable() {
            @Override
            public void run() {
                if (dialog == null) {
                    dialog = new Dialog(current_activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.fingerprint);

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }

                WebView payload_modal = (WebView) dialog.findViewById(R.id.fingerprint_webview);
                payload_modal.setWebContentsDebuggingEnabled(true);

                WebSettings settings = payload_modal.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setAllowContentAccess(true);

                payload_modal.addJavascriptInterface(new Object() {
                    /**
                     * Process result from JS
                     *
                     * @param result
                     */
                    @JavascriptInterface
                    public void processResult(String result) {
                        hideDialog();
                        gettable.toGet(result);
                    }
                }, "Android");

                payload_modal.loadData(getPayload(merchant_id, org_id), "text/html", "utf8");

                dialog.show();
            }
        });
    }

    /**
     * Hide webview dialog
     */
    public void hideDialog() {
        if (this.dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * Get Cybersource fingerprint ID
     *
     * @param activity
     * @param merchant_id
     * @param org_id
     * @param callback
     */
    public static void getFingerprint(Activity activity, String merchant_id, String org_id, Gettable callback) {
        (new CybersourceManager(activity)).showDialog(merchant_id, org_id, callback);
    }

    /**
     * Get HTML builder payload
     *
     * @param merchant_id
     * @param org_id
     * @return
     */
    private String getPayload(String merchant_id, String org_id) {
        return "<html><body><script>" +
                "initCybersourceSetup('"+merchant_id+"', '"+org_id+"');" +
                "function initCybersourceSetup(merchantID, orgID) {" +
                "var sessionID = new Date().getTime();" +
                "var metrixContainer = document.createElement('div'); metrixContainer.id = 'metrics'; metrixContainer.style.position = 'absolute'; metrixContainer.style.top = 0; metrixContainer.style.opacity = 0; metrixContainer.style.width = '1px'; metrixContainer.style.height = '1px'; document.body.appendChild(metrixContainer);" +
                "var paragraphTM = document.createElement('p'); paragraphTM.styleSheets = `background:url(https://h.online-metrix.net/fp/clear.png?org_id=${orgID}&session_id=${merchantID}${sessionID}&m=1)`; metrixContainer.appendChild(paragraphTM);" +
                "var img = document.createElement('img'); img.src = `https://h.online-metrix.net/fp/clear.png?org_id=${orgID}&session_id=${merchantID}${sessionID}&m=2`; img.alt = ''; metrixContainer.appendChild(img);" +
                "var objectTM = document.createElement('object'); objectTM.data = `https://h.online-metrix.net/fp/fp.swf?org_id=${orgID}&session_id=${merchantID}${sessionID}`; objectTM.type = 'application/x-shockwave-flash'; objectTM.width = '1'; objectTM.height = '1'; objectTM.id = 'thm_fp';" +
                "var param = document.createElement('param'); param.name = 'movie'; param.value = `https://h.online-metrix.net/fp/fp.swf?org_id=${orgID}&session_id=${merchantID}${sessionID}`;" +
                "objectTM.appendChild(param);" +
                "metrixContainer.appendChild(objectTM);" +
                "var tmscript = document.createElement('script'); tmscript.src = `https://h.online-metrix.net/fp/tags.js?org_id=${orgID}&session_id=${merchantID}${sessionID}`; tmscript.type = 'text/javascript'; document.body.appendChild(tmscript);" +
                "typeof Android !== 'undefined' ?  Android.processResult(`${sessionID}`) : console.log(`${sessionID}`);" +
                "}</script></body></html>";
    }
}
