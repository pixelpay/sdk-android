package com.pixel.sdk.mobile.services;

import android.app.Activity;

import com.pixel.sdk.base.ServiceBehaviour;
import com.pixel.sdk.requests.CardTokenization;
import com.pixel.sdk.mobile.base.AsyncResponse;
import com.pixel.sdk.mobile.utils.Version;
import com.pixel.sdk.models.Settings;

public class Tokenization extends ServiceBehaviour {

	/**
	 * Android project activity
	 */
	protected Activity current_activity;

	/**
	 * Parent Transaction service
	 */
	private final com.pixel.sdk.services.Tokenization parent_service;

	/**
	 * Initialize instance
	 *
	 * @param settings
	 */
	public Tokenization(Settings settings, Activity current_activity) {
		super(settings);
		this.current_activity = current_activity;
		this.parent_service = new com.pixel.sdk.services.Tokenization(settings);

		if (this.settings.sdk == null) {
			this.settings.sdk = "sdk-android";
		}

		if (this.settings.sdk_version == null) {
			this.settings.sdk_version = Version.SDK_VERSION;
		}
	}

	/**
	 * Vault credit/debit card and obtain a token card identifier (T-* format)
	 */
	public AsyncResponse vaultCard(CardTokenization card) {
		return new AsyncResponse(() -> this.parent_service.vaultCard(card), this.current_activity).withLoading();
	}

	/**
	 * Update credit/debit card by token card identifier
	 */
	public AsyncResponse updateCard(String token, CardTokenization card) {
		return new AsyncResponse(() -> this.parent_service.updateCard(token, card), this.current_activity)
				.withLoading();
	}

	/**
	 * Show credit/debit card metadata by token card identifier
	 */
	public AsyncResponse showCard(String token) {
		return new AsyncResponse(() -> this.parent_service.showCard(token), this.current_activity);
	}

	/**
	 * Show credit/debit cards metadata by tokens card identifiers
	 */
	public AsyncResponse showCards(String[] tokens) {
		return new AsyncResponse(() -> this.parent_service.showCards(tokens), this.current_activity);
	}

	/**
	 * Delete credit/debit card metadata by token card identifier
	 */
	public AsyncResponse deleteCard(String token) {
		return new AsyncResponse(() -> this.parent_service.deleteCard(token), this.current_activity).withLoading();
	}
}
