package com.pixel.sdk.mobile.exceptions;

public class IntegrationException extends Exception {
    public IntegrationException(String message) {
        super(message);
    }
}