package com.pixel.sdk.mobile.requests;

import com.pixel.sdk.base.RequestBehaviour;
import com.pixel.sdk.requests.PaymentTransaction;
import com.pixel.sdk.requests.SaleTransaction;

public class LookupTransaction extends RequestBehaviour {
	/**
	 * Tokenized card identifier (T-* format)
	 */
	public String card_token;

	/**
	 * Card number or PAN
	 */
	public String card_number;

	/**
	 * Card expire year/month date (YYMM)
	 */
	public String card_expire;

	/**
	 * Customer billing address
	 */
	public String billing_address;

	/**
	 * Customer billing country alpha-2 code (ISO 3166-1)
	 */
	public String billing_country;

	/**
	 * Customer billing state alpha code (ISO 3166-2)
	 */
	public String billing_state;

	/**
	 * Customer billing city
	 */
	public String billing_city;

	/**
	 * Customer billing postal code
	 */
	public String billing_zip;

	/**
	 * Customer billing phone
	 */
	public String billing_phone;

	/**
	 * Order customer name
	 */
	public String customer_name;

	/**
	 * Order customer email
	 */
	public String customer_email;

	/**
	 * Order ID
	 */
	public String order_id;

	/**
	 * Order currency code alpha-3
	 */
	public String order_currency;

	/**
	 * Order total amount
	 */
	public String order_amount;

	/**
	 * Indicates the maximum number of authorizations for installment payments.
	 */
	public String installment;

	/**
	 * Cardinal session reference
	 */
	public String reference;

	/**
	 * Transaction Device identifier
	 */
	public String device_channel = "SDK";

	/**
	 * The exact content of the HTTP user agent header.
	 */
	public String user_agent = "PixelPay HttpClient/3 (MobileSDK) Android/Java";

	/**
	 * Cast PaymentTransaction to LookupTransaction
	 */
	public void fromPaymentTransaction(PaymentTransaction request) {
		this.card_token = request.card_token;
		this.card_number = request.card_number;
		this.card_expire = request.card_expire;

		this.billing_address = request.billing_address;
		this.billing_country = request.billing_country;
		this.billing_state = request.billing_state;
		this.billing_city = request.billing_city;
		this.billing_zip = request.billing_zip;
		this.billing_phone = request.billing_phone;

		this.customer_name = request.customer_name;
		this.customer_email = request.customer_email;

		this.order_id = request.order_id;
		this.order_currency = request.order_currency;
		this.order_amount = request.order_amount;

		if (request instanceof SaleTransaction) {
			this.installment = ((SaleTransaction) request).installment_months;
		}
	}
}
