package com.pixel.sdk.demo;

import com.pixel.sdk.resources.Locations;
import java.util.Map;
import java.util.HashMap;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.pixel.sdk.demo", appContext.getPackageName());
    }

    @Test
    public void statesListIsCorrect() {
        Map<String, String> states = Locations.statesList("GT");

        Map<String, String> expectedStates = new HashMap<>();
        expectedStates.put("GT-16", "Alta Verapaz");
        expectedStates.put("GT-15", "Baja Verapaz");
        expectedStates.put("GT-04", "Chimaltenango");
        expectedStates.put("GT-20", "Chiquimula");
        expectedStates.put("GT-02", "El Progreso");
        expectedStates.put("GT-05", "Escuintla");
        expectedStates.put("GT-01", "Guatemala");
        expectedStates.put("GT-13", "Huehuetenango");
        expectedStates.put("GT-18", "Izabal");
        expectedStates.put("GT-21", "Jalapa");
        expectedStates.put("GT-22", "Jutiapa");
        expectedStates.put("GT-17", "Peten");
        expectedStates.put("GT-09", "Quetzaltenango");
        expectedStates.put("GT-14", "Quiche");
        expectedStates.put("GT-11", "Retalhuleu");
        expectedStates.put("GT-03", "Sacatepequez");
        expectedStates.put("GT-12", "San Marcos");
        expectedStates.put("GT-06", "Santa Rosa");
        expectedStates.put("GT-07", "Solola");
        expectedStates.put("GT-10", "Suchitepequez");
        expectedStates.put("GT-08", "Totonicapan");
        expectedStates.put("GT-19", "Zacapa");

        assertEquals(expectedStates, states);
    }
}