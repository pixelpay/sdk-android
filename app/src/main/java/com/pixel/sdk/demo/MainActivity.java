package com.pixel.sdk.demo;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.pixel.sdk.base.Helpers;
import com.pixel.sdk.demo.databinding.PixelFormBinding;
import com.pixel.sdk.mobile.libraries.CybersourceManager;
import com.pixel.sdk.requests.AuthTransaction;
import com.pixel.sdk.requests.CaptureTransaction;
import com.pixel.sdk.requests.CardTokenization;
import com.pixel.sdk.requests.SaleTransaction;
import com.pixel.sdk.requests.StatusTransaction;
import com.pixel.sdk.requests.VoidTransaction;
import com.pixel.sdk.mobile.services.Tokenization;
import com.pixel.sdk.mobile.services.Transaction;
import com.pixel.sdk.models.Billing;
import com.pixel.sdk.models.Card;
import com.pixel.sdk.models.Item;
import com.pixel.sdk.models.Order;
import com.pixel.sdk.models.Settings;
import com.pixel.sdk.responses.SuccessResponse;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

	private AppBarConfiguration appBarConfiguration;
	private PixelFormBinding binding;

	private Settings merchant;
	private Transaction transactionService;
	private Tokenization tokenizationService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = PixelFormBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		Spinner spin = (Spinner) findViewById(R.id.list_cards);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.cards_available,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin.setAdapter(adapter);
		spin.setOnItemSelectedListener(this);

		this.setupMerchant();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		// noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void setupMerchant() {
		this.merchant = new Settings();
		merchant.setupSandbox();

		this.transactionService = new Transaction(this.merchant, this);
		this.tokenizationService = new Tokenization(this.merchant, this);
	}

	public Billing getBillingModel() {
		Billing billing = new Billing();
		billing.address = "Bo Andes";
		billing.city = "San Pedro Sula";
		billing.country = "HN";
		billing.state = "HN-CR";
		billing.phone = "999-9999999";

		return billing;
	}

	public Card getCardModel() {
		Card card = new Card();
		EditText cardNumber = findViewById(R.id.input_card_number);
		EditText cardMonth = findViewById(R.id.input_card_month);
		EditText cardYear = findViewById(R.id.input_card_year);
		EditText cardCvv = findViewById(R.id.input_card_cvv);

		card.number = (cardNumber.getText().toString() == "") ? "" : cardNumber.getText().toString();
		card.cardholder = "TEST CARD";

		if (!cardMonth.getText().toString().matches("")) {
			card.expire_month = Integer.parseInt(cardMonth.getText().toString());
		}

		if (!cardMonth.getText().toString().matches("")) {
			card.expire_year = Integer.parseInt(cardYear.getText().toString());
		}

		card.cvv2 = cardCvv.getText().toString();

		return card;
	}

	public Order getOrderModel() {
		Order order = new Order();

		order.id = UUID.randomUUID().toString();
		order.amount = 1;
		order.currency = "HNL";
		order.customer_email = "carlos@pixel.hn";
		order.customer_name = "Carlos Agaton";

		Item item = new Item();
		item.code = "00001";
		item.title = "Example product";
		item.price = 1;

		order.addItem(item);

		return order;
	}

	public String printResult(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			return jsonObject.toString(2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public void getStatus(View view) {
		StatusTransaction statusTx = new StatusTransaction();
		EditText uuid = findViewById(R.id.input_uuid);
		EditText inputResponse = findViewById(R.id.input_response);

		statusTx.payment_uuid = uuid.getText().toString();

		this.transactionService.getStatus(statusTx).then((response) -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(this.printResult(response.toJson()));
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch((ex) -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doCapture(View view) {
		CaptureTransaction capture = new CaptureTransaction();
		EditText uuid = findViewById(R.id.input_uuid);
		EditText inputResponse = findViewById(R.id.input_response);

		capture.payment_uuid = uuid.getText().toString();
		capture.transaction_approved_amount = "1.00";

		this.transactionService.doCapture(capture).then((response) -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch((ex) -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doVoid(View view) {
		this.merchant.setupPlatformUser(
				"2292531aaa94f1e74fab7a0bc564328100f39ee25c76dcb1ebd5e46acb5f133a7c682bea7c251407cf41b46c35812b3478c00f5b6e55923eefd1d29e8517389a");
		VoidTransaction voidTx = new VoidTransaction();
		EditText uuid = findViewById(R.id.input_uuid);
		EditText inputResponse = findViewById(R.id.input_response);

		voidTx.payment_uuid = uuid.getText().toString();
		voidTx.void_reason = "Test Transaction";

		this.transactionService.doVoid(voidTx).then((response) -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch((ex) -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doSale(View view) {
		EditText inputToken = findViewById(R.id.input_token);
		Switch authentication = (Switch) findViewById(R.id.authentication);

		String token = inputToken.getText().toString();

		SaleTransaction sale = new SaleTransaction();
		sale.setOrder(this.getOrderModel());
		sale.authentication_request = false;

		if (authentication.isChecked()) {
			System.out.println("is checked");
			sale.withAuthenticationRequest();
		}

		if (!token.matches("") && !token.startsWith("T-")) {
			sale.setCardToken(token);
		} else {
			sale.setCard(this.getCardModel());
			sale.setBilling(this.getBillingModel());
		}

		EditText inputResponse = findViewById(R.id.input_response);

		transactionService.doSale(sale).then((response) -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch((ex) -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doAuthorize(View view) {
		EditText inputToken = findViewById(R.id.input_token);
		Switch authentication = (Switch) findViewById(R.id.authentication);

		String token = inputToken.getText().toString();

		AuthTransaction auth = new AuthTransaction();
		auth.setBilling(this.getBillingModel());
		auth.setOrder(this.getOrderModel());

		if (authentication.isChecked()) {
			System.out.println("is checked");
			auth.withAuthenticationRequest();
		}

		if (!token.matches("") && !token.startsWith("T-")) {
			auth.setCardToken(token);
		} else {
			auth.setCard(this.getCardModel());
		}

		EditText inputResponse = findViewById(R.id.input_response);

		transactionService.doAuth(auth).then((response) -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch((ex) -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doVault(View view) {
		CardTokenization card = new CardTokenization();
		card.setCard(this.getCardModel());
		card.setBilling(this.getBillingModel());

		EditText inputResponse = findViewById(R.id.input_response);
		EditText inputToken = findViewById(R.id.input_token);

		this.tokenizationService.vaultCard(card).then(response -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
				Object token = response.getData("token");
				inputToken.setText(token.toString());
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch(ex -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doUpdate(View view) {
		CardTokenization card = new CardTokenization();
		card.setCard(this.getCardModel());
		card.setBilling(this.getBillingModel());

		EditText inputToken = findViewById(R.id.input_token);
		EditText inputResponse = findViewById(R.id.input_response);

		this.tokenizationService.updateCard(inputToken.getText().toString(), card).then(response -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch(ex -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doShow(View view) {
		EditText inputToken = findViewById(R.id.input_token);
		EditText inputResponse = findViewById(R.id.input_response);

		this.tokenizationService.showCard(inputToken.getText().toString()).then(response -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch(ex -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	public void doDelete(View view) {
		EditText inputToken = findViewById(R.id.input_token);
		EditText inputResponse = findViewById(R.id.input_response);

		this.tokenizationService.deleteCard(inputToken.getText().toString()).then(response -> {
			String message = "";

			if (response instanceof SuccessResponse) {
				message = response.message + " - Status: " + response.getData("status");
			} else {
				message = "Response message: " + response.message;
			}

			inputResponse.setText(response.toJson());
			Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
		}).thenCatch(ex -> {
			Snackbar.make(view, "Err: " + ex.getMessage(), Snackbar.LENGTH_LONG).show();
		}).start();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		String number = parent.getItemAtPosition(position).toString();

		EditText inputCardNumber = findViewById(R.id.input_card_number);
		EditText inputCardMonth = findViewById(R.id.input_card_month);
		EditText inputCardYear = findViewById(R.id.input_card_year);
		EditText inputCardCvv = findViewById(R.id.input_card_cvv);

		int year = Calendar.getInstance().get(Calendar.YEAR) + 3;

		inputCardNumber.setText(number);
		inputCardMonth.setText("11");
		inputCardYear.setText(String.valueOf(year));
		inputCardCvv.setText("999");
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}
}
