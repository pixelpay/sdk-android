# Android Java SDK

## ¿Cómo publicar?

- Cambiar versión de release en library/build.gradle.
- Cambiar versión en library/src/main/java/com/pixel/sdk/mobile/utils/Version.java.
- Agregar al CHANGELOG.md las descripciones del cambio.
- Crear un commit con los cambios y agregar una etiqueta con el número de versión y hacer push con los tags.

## ¿Cómo actualizar las librerias de Cardinal?

- Ejecutar en la carpeta raiz del proyecto `sh pull_cardinal.sh {ultima_version_de_cardinal}`, se puede obtener la ultima version [aqui](https://cardinaldocs.atlassian.net/wiki/spaces/CMSDK/pages/11862033/Cardinal+Mobile+SDK+-+Android) ej. `sh pull_cardinal.sh 2.2.7-3`
- Cambiar versión de release en cardinal/pom.xml.
- Crear un commit con los cambios y agregar una etiqueta con el número de versión y hacer push con los tags.
- Se debe de ingresar al sitio jitpack.io y revisar si esta publicado el repositorio buscando `org.bitbucket.pixelpay/maven-pixelpay-sdk-3ds`